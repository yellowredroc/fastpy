port=$1
ps -ef | grep proxy | grep $port | awk '{print $2}' | xargs -i{} kill -9 {}
